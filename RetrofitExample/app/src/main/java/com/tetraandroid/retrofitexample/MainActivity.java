package com.tetraandroid.retrofitexample;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.tetraandroid.retrofitexample.http.TwitchAPI;
import com.tetraandroid.retrofitexample.http.apimodel.Top;
import com.tetraandroid.retrofitexample.http.apimodel.Twitch;
import com.tetraandroid.retrofitexample.root.App;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Inject
    TwitchAPI twitchAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Logger.addLogAdapter(new AndroidLogAdapter());
        ((App) getApplication()).getComponent().inject(this);

        Call<Twitch> call = twitchAPI.getTopGames(BuildConfig.TWT_CLIENT_ID);

        call.enqueue(new Callback<Twitch>() {
            @Override
            public void onResponse(@NonNull Call<Twitch> call, @NonNull Response<Twitch> response) {
                Twitch body = response.body();
                List<Top> gameList;
                if (body != null) {
                    gameList = body.getTop();
                    StringBuilder sb = new StringBuilder();
                    for (Top top : gameList) {
                        sb.append(top.getGame().getName()).append("\n");
                    }
                    Logger.t("Games").i(sb.toString());
                } else {
                    Logger.t(MainActivity.class.getSimpleName()).w("Body received, but empty");
                }
            }

            @Override
            public void onFailure(@NonNull Call<Twitch> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });

    }
}

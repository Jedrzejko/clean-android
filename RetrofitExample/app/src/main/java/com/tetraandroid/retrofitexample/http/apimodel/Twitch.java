package com.tetraandroid.retrofitexample.http.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tetraandroid.retrofitexample.utils.CollectionsUtils;

import java.util.Collections;
import java.util.List;

public class Twitch {

    @SerializedName("_total")
    @Expose
    private Integer total;
    @SerializedName("top")
    @Expose
    private List<Top> top = null;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Top> getTop() {
        return this.top != null ? this.top.subList(0, getTopNumber()) : Collections.<Top>emptyList();
    }

    private int getTopNumber() {
        return CollectionsUtils.isEmpty(this.top) ? 0 : this.top.size() > 20 ? 20 : this.top.size();
    }

    public void setTop(List<Top> top) {
        this.top = top;
    }

}

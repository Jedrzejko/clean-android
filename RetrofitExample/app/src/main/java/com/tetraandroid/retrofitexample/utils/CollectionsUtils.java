package com.tetraandroid.retrofitexample.utils;

import java.util.List;

/**
 * @author bovquier
 * on 12.09.2017.
 */

public class CollectionsUtils {
    public static boolean isEmpty(List list) {
        int nullsCounter = 0;
        if (list == null || list.isEmpty()) {
            return true;
        } else {
            for (Object o : list) {
                if (o == null) nullsCounter++;
            }
        }
        return nullsCounter == list.size();
    }
}

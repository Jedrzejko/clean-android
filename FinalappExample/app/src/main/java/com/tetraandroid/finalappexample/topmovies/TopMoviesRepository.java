package com.tetraandroid.finalappexample.topmovies;

import com.tetraandroid.finalappexample.http.MoreInfoApiService;
import com.tetraandroid.finalappexample.http.MovieApiService;
import com.tetraandroid.finalappexample.http.apimodel.OmdbApi;
import com.tetraandroid.finalappexample.http.apimodel.Result;
import com.tetraandroid.finalappexample.http.apimodel.TopRated;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.operators.observable.ObservableConcatMap;

public class TopMoviesRepository implements Repository {

    private static final long STALE_MS = 20 * 1000; // Data is stale after 20 seconds
    private static int staticCountry;
    private MovieApiService movieApiService;
    private MoreInfoApiService moreInfoApiService;
    private List<String> countries;
    private List<Result> results;
    private long timestamp;

    TopMoviesRepository(MovieApiService movieApiService, MoreInfoApiService moreInfoApiService) {
        this.moreInfoApiService = moreInfoApiService;
        this.timestamp = System.currentTimeMillis();
        this.movieApiService = movieApiService;
        countries = new ArrayList<>();
        results = new ArrayList<>();
    }

    private OmdbApi getMockedOmdbValue() {
        OmdbApi value = new OmdbApi();
        value.setCountry(getNextStaticCountry());
        return value;
    }

    private static String getNextStaticCountry() {
        return String.format(Locale.getDefault(), "StaticCountry%d", staticCountry++);
    }

    private boolean isUpToDate() {
        return System.currentTimeMillis() - timestamp < STALE_MS;
    }

    @Override
    public Observable<Result> getResultsFromMemory() {
        if (isUpToDate()) {
            return Observable.fromIterable(results);
        } else {
            timestamp = System.currentTimeMillis();
            results.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<Result> getResultsFromNetwork() {
        return movieApiService.getTopRatedMovies(1).concatWith(movieApiService.getTopRatedMovies(2))
                .concatWith(movieApiService.getTopRatedMovies(3))
                .concatMap(new Function<TopRated, ObservableSource<? extends Result>>() {
                    @Override
                    public ObservableSource<Result> apply(TopRated topRated) throws Exception {
                        return Observable.fromIterable(topRated.results);
                    }
                }).doOnNext(new Consumer<Result>() {
                    @Override
                    public void accept(Result result) throws Exception {
                        results.add(result);
                    }
                });
    }

    @Override
    public Observable<String> getCountriesFromMemory() {
        if (isUpToDate()) {
            return Observable.fromIterable(countries);
        } else {
            timestamp = System.currentTimeMillis();
            countries.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<String> getCountriesFromNetwork() {
        Observable<Result> resultsFromNetwork = getResultsFromNetwork();
        return resultsFromNetwork.concatMap(new Function<Result, ObservableSource<OmdbApi>>() {
            @Override
            public ObservableSource<OmdbApi> apply(Result result) throws Exception {
                return moreInfoApiService.getCountry(result.title);
            }
        }).concatMapDelayError().concatMap(new Function<OmdbApi, ObservableSource<?
                extends
                String>>() {
            @Override
            public ObservableSource<? extends String> apply(OmdbApi omdbApi) throws Exception {
                return Observable.just(omdbApi.getCountry());
            }
        }).doOnNext(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                countries.add(s);
            }
        });
    }


    @Override
    public Observable<String> getCountryData() {
        return getCountriesFromMemory().switchIfEmpty(getCountriesFromNetwork());
    }

    @Override
    public Observable<Result> getResultData() {
        return getResultsFromMemory().switchIfEmpty(getResultsFromNetwork());
    }
}

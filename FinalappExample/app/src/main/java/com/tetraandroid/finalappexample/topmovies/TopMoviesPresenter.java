package com.tetraandroid.finalappexample.topmovies;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class TopMoviesPresenter implements TopMoviesActivityMVP.Presenter {

    private final CompositeDisposable disposables = new CompositeDisposable();
    private TopMoviesActivityMVP.View view;
    private TopMoviesActivityMVP.Model model;

    TopMoviesPresenter(TopMoviesActivityMVP.Model model) {
        this.model = model;
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    @Override
    public void loadData() {
        disposables.add(model.result()
                                // Run on a background thread
                                .subscribeOn(Schedulers.io())
                                // Be notified on the main thread
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableObserver<ViewModel>() {
                                    @Override
                                    public void onNext(ViewModel viewModel) {
                                        if (view != null) {
                                            view.updateData(viewModel);
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Logger.e(e, "Error getting movies");
                                        if (view != null) {
                                            view.showSnackbar("Error getting movies");
                                        }
                                    }

                                    @Override
                                    public void onComplete() {
                                        Logger.d("onComplete()");
                                    }
                                }));
    }

    @Override
    public void rxUnsubscribe() {
        if (disposables != null && !disposables.isDisposed()) disposables.dispose();
    }

    @Override
    public void setView(TopMoviesActivityMVP.View view) {
        this.view = view;
    }
}

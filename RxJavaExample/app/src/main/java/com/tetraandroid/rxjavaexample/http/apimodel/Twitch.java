package com.tetraandroid.rxjavaexample.http.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tetraandroid.rxjavaexample.utils.CollectionsUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Twitch {

    @SerializedName("_total")
    @Expose
    private Integer total;
    @SerializedName("_links")
    @Expose
    private Links links;
    @SerializedName("top")
    @Expose
    private List<Top> top = new ArrayList<Top>();

    /**
     * @return The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total The _total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * @return The links
     */
    public Links getLinks() {
        return links;
    }

    /**
     * @param links The _links
     */
    public void setLinks(Links links) {
        this.links = links;
    }

    /**
     * @return The top
     */
    public List<Top> getTop() {
        return this.top != null ? this.top.subList(0, getTopNumber()) : Collections.<Top>emptyList();
    }

    private int getTopNumber() {
        final int top = 5;
        return CollectionsUtils.isEmpty(this.top) ? 0 : this.top.size() > top ? top : this.top.size();
    }

    /**
     * @param top The top
     */
    public void setTop(List<Top> top) {
        this.top = top;
    }

}
